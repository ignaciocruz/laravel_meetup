<!-- resources/views/proyectos/index.blade.php -->


@extends('templates.app')
 
@section('content')
    <h2>Proyectos</h2>
 
    @if ( !$proyectos->count() )
        You have no projects
    @else
                           
        <div class="row">
            <div class="col-md-12">
                <table class="table">
                    <tr>
                        <th>Nombre</th>
                        <th>Slug</th>
                        <th>Última actualización</th>
                        <th>Acción</th>
                    </tr>

                  @foreach( $proyectos as $proyecto )
                    <tr>
                        <td><a href="{{ route('proyectos.show', $proyecto->id) }}"> {{ $proyecto->nombre }}</a></td>
                        <td>{{ $proyecto->slug }}</td>
                        <td>{{ $proyecto->updated_at }}</td>
                        <td>
                        {{ link_to_route('proyectos.edit', 'Edit', [$proyecto->id] , ['class' => 'btn btn-info'])   }}
                        {!! Form::submit('Delete', array('class' => 'btn btn-danger')) !!}
                        </td>
                    </tr>
                    @endforeach

                </table>
            </div>
        </div>
    @endif

 
    <p>
        {!! link_to_route('proyectos.create', 'Create Project') !!}
    </p>
@endsection